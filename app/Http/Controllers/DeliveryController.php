<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\Truck;
use App\Trip;
use App\Delivery;

class DeliveryController extends Controller
{
    // emptyDashboard
    public function fetchDashboard(){
        // $array = collection([]);
        // return $array;
    }
	// Fetch
	public function fetchDelivery(){
 		return Delivery::all();
	}

    public function index(){


        $drivers = Driver::where("status_id",1)->get();
        $trips = Trip::all();
        $openTrips = Trip::where("quantity",">",0)->get();

        // $deliveries = DB::table("driver_trip")->get();
        $tractors = Truck::where([["type_id",1],["status_id",3]])->get();
        $trailers = Truck::where([["type_id",2],["status_id",3]])->get();

        // dd($deliveries);

        // $deliveries=DB::table("trips")->join("driver_trip","trips.id","=","driver_trip.trip_id")->join("drivers","drivers.id","=","driver_trip.driver_id")->get();

        $deliveries=Delivery::whereNull("deleted_at")->get();

        return view("dashboard")->with(compact("drivers","trips","openTrips","tractors","trailers","deliveries"));
    }

    public function addDelivery(Request $req){

        $trip = Trip::find($req->tripId);
        if($trip->quantity<=0){
            return redirect()->back();
        }

        $trip->quantity -= 1;
        $trip->save();

    	// $driver = Driver::find($req->driverId)->trips()->attach("$req->tripId",["status_id"=>5]);

        $driver=Driver::find($req->driverId);
        $driver->status_id = 2;
        $driver->save();

        $newDelivery = new Delivery;

        $newDelivery->driver_id = $req->driverId; 
        $newDelivery->trip_id = $req->tripId;
        $newDelivery->status_id = 5;
        // dd($newDelivery);
        $newDelivery->save();

    	return redirect()->back();
    }

    public function updateDelivery($id, Request $req){
        $this->validate($req,array(
            "tractorId" => "required",
            "trailerId" => "required"
        ));

        $delivery = Delivery::find($id);
        $tractor = Truck::find($req->tractorId);
        $trailer = Truck::find($req->trailerId);

        $tractor->status_id = 2;
        $trailer->status_id = 2;

        $tractor->save();
        $trailer->save();

        $delivery->truck_tractor_id = $req->tractorId;
        $delivery->truck_trailer_id = $req->trailerId;
        $delivery->status_id = 6;
        $delivery->save();

        return redirect()->back();
    }

    public function destroyDelivery($id){
        $delivery = Delivery::find($id);
        $driver = Driver::find($delivery->driver_id);
        $driver->status_id = 1;
        $driver->save();
        $delivery->delete();
        return redirect()->back();
    }
}
