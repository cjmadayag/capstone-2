<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\Contact;
use App\Address;
use App\User;

class DriverController extends Controller
{
    // fetch
    public function fetchDriver(){
        return Driver::where("status_id",1)->get();
    }
    // Get Driver
    public function getDriver($id){
        return Driver::find($id);
    }

    // Add Driver View
    public function create(){
        $handlers = User::whereIn("role_id",[1,3])->get();

        return view("adminviews.adddriver")->with(compact("handlers"));
    }
    // Add Driver
    public function store(Request $req){
        $newDriver = new Driver;

        $this->validate($req, array(
            "name" => "required"
        ));

        $newDriver->name = $req->name;
        $newDriver->contact = $req->contact;
        $newDriver->address = $req->address;
        $newDriver->status_id = 1;
        $newDriver->user_id = $req->user_id;
        $newDriver->save();

        return redirect("/dashboard");
    }
    // View Drivers
    public function index(){
        $drivers = Driver::all();
        return view("adminviews.drivers")->with(compact("drivers"));
    }
    // Edit Driver
    public function edit($id){
        $driver = Driver::find($id);
        $handlers = User::whereIn("role_id",[1,3])->get();
        return view("adminviews.editdriver")->with(compact("handlers","driver"));
    }
    // Update Driver
    public function update($id, Request $req){
        $driver = Driver::find($id);

        $this->validate($req, array(
            "name" => "required"
        ));

        $driver->name = $req->name;
        $driver->contact = $req->contact;
        $driver->address = $req->address;
        $driver->user_id = $req->user_id;
        $driver->save();

        return redirect("/drivers");
    }
    // Delete Driver
    public function destroy($id){
        $driver = Driver::find($id);
        $driver->delete();

        return redirect()->back();
    }
}
