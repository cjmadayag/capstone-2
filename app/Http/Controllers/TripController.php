<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trip;

class TripController extends Controller
{
    // fetch
    public function fetchTrip(){
        return Trip::all();
    }
    // Add Trip View
    public function create(){
        return view("adminviews.addtrip");
    }
    // Add Trip
    public function store(Request $req){
        $trip = new Trip;

        $this->validate($req,array(
            "source" => "required",
            "destination" => "required"
        ));

        $trip->source = $req->source;
        $trip->destination = $req->destination;
        $trip->quantity = 0;
        $trip->save();

        return redirect("/dashboard");
    }
    // View Trips
    public function index(){
        $trips = Trip::all();

        return view("adminviews.trips")->with(compact("trips"));
    }
    // Edit Trip
    public function edit($id){
        $trip = Trip::find($id);

        return view("adminviews.edittrip")->with(compact("trip"));
    }
    // Update Trip
    public function update($id, Request $req){
        $trip = Trip::find($id);

        $this->validate($req,array(
            "source" => "required",
            "destination" => "required"
        ));
        
        $trip->source = $req->source;
        $trip->destination = $req->destination;
        $trip->save();

        return redirect("/trips");
    }
    // Delete Trip
    public function destroy($id){
        $trip = Trip::find($id);
        $trip->delete();

        return redirect()->back();
    }
    // Add Qty to Trip
    public function addQtyToTrip($id,$opt){
        $trip = Trip::find($id);
        if($opt == "-" && $trip->quantity > 0){
            $trip->quantity -= 1;
        }else{
            $trip->quantity += 1;
        }
        $trip->save();

        return redirect()->back();
    }
}
