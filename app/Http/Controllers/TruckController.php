<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\User;
use App\Truck;

class TruckController extends Controller
{
    // Add Truck View
    public function create(){
        $types = Type::wherein("name",["Tractor","Trailer"])->get();
        $handlers = User::whereIn("role_id",[1,3])->get();

        return view("adminviews.addtruck")->with(compact("types","handlers"));
    }
    // Add Truck
    public function store(Request $req){
        $newTruck = new Truck;

        $this->validate($req, array(
            "plate"=>"required"
        ));
        
        $newTruck->plate = $req->plate;
        $newTruck->status_id = 3;
        $newTruck->type_id = $req->type_id;
        $newTruck->user_id = $req->user_id;
        $newTruck->save();

        return redirect("/dashboard");
    }
    // View Trucks
    public function index(){
        $trucks = Truck::all();
        return view("adminviews.trucks")->with(compact("trucks"));
    }
    // Edit Truck
    public function edit($id){
        $truck = Truck::find($id);
        $types = Type::wherein("name",["Tractor","Trailer"])->get();
        $handlers = User::whereIn("role_id",[1,3])->get();

        return view("adminviews.edittruck")->with(compact("truck","types","handlers"));
    }
    // Update Truck
    public function update($id, Request $req){
        $truck = Truck::find($id);

        $this->validate($req, array(
            "plate" => "required"
        ));
        
        $truck->plate = $req->plate;
        $truck->type_id = $req->type_id;
        $truck->user_id = $req->user_id;
        $truck->save();

        return redirect("/trucks");
    }
    // Delete Truck
    public function destroy($id){
        $truck = Truck::find($id);
        $truck->delete();

        return redirect()->back();
    }
}
