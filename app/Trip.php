<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    public function drivers(){
    	return $this->belongsToMany("App\Driver")->withPivot("truck_tractor_id","truck_trailer_id","status_id");
    }
}
