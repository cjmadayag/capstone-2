<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    public function status(){
        return $this->belongsTo("App\Status");
    }
    
    public function user(){
        return $this->belongsTo("App\User");
    }

    public function trips(){
    	return $this->belongsToMany("App\Trip")->withPivot("truck_tractor_id","truck_trailer_id","status_id");
    }
}
