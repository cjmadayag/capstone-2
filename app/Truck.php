<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    public function status(){
        return $this->belongsTo("App\Status");
    }

    public function type(){
        return $this->belongsTo("App\Type");
    }

    public function user(){
        return $this->belongsTo("App\User");
    }
}
