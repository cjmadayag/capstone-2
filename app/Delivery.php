<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Delivery extends Model
{
    public function driver(){
    	return $this->belongsTo("App\Driver");
    }
    public function trip(){
    	return $this->belongsTo("App\Trip");
    }
    public function truck_tractor(){
    	return $this->belongsTo("App\Truck");
    }
    public function truck_trailer(){
    	return $this->belongsTo("App\Truck");
    }
    public function status(){
    	return $this->belongsTo("App\Status");
    }
    use SoftDeletes;
}
