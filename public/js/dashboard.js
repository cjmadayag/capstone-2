
// const dashTable = document.querySelectorAll(".dashTable");

// fetch("/fetchDashboard").then(res=>res.text()).then(res=>{
//     // console.log(res);
//     // let table = document.getElementById("welcome");
//     // if(res.length!=null){
//     //     table.classList.remove("hidden");
//     // }else{
//     //     table.classList.add("hidden");
//     // }
// })

fetch("/fetchDelivery").then(res=>res.json()).then(res=>{
    let table = document.getElementById("deliveryTable");
    if(res.length==0){
        table.classList.add("hidden");
    }else{
        table.classList.remove("hidden");
    }
});

fetch("/fetchDriver").then(res=>res.json()).then(res=>{
    let table = document.getElementById("driverTable");
    if(res.length==0){
        table.classList.add("hidden");
    }else{
        table.classList.remove("hidden");
    }
});

fetch("/fetchTrip").then(res=>res.json()).then(res=>{
    let table = document.getElementById("tripTable");
    if(res.length==0){
        table.classList.add("hidden");
    }else{
        table.classList.remove("hidden");
    }
});

function assign(id){
    fetch("/getDriver/"+id).then(res=>res.json()).then(res=>{
        let modalId = document.getElementById("modal");

        modalId.previousElementSibling.textContent=res.name;
        document.getElementById("assign").previousElementSibling.previousElementSibling.value=res.id;
        modalId.parentElement.parentElement.classList.remove("hidden");

        modalId.lastElementChild.firstElementChild.firstElementChild.lastElementChild.addEventListener("click",()=>{
            modalId.parentElement.parentElement.classList.add("hidden");
        });
    });
};

let radioTrip = document.querySelectorAll("input[name=trip_id]");
radioTrip.forEach((input)=>{

    input.addEventListener("click",()=>{
       document.getElementById("assign").previousElementSibling.value = input.value;
    })
})


