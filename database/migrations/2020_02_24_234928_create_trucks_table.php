<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("plate");
            $table->unsignedBigInteger("status_id");
            $table->unsignedBigInteger("type_id");
            $table->unsignedBigInteger("remark_id")->nullable();
            $table->unsignedBigInteger("user_id");
            $table->timestamps();

            $table->
            foreign("status_id")->
            references("id")->
            on("statuses")->
            onDelete("restrict")->
            onUpdate("cascade");

            $table->
            foreign("type_id")->
            references("id")->
            on("types")->
            onDelete("restrict")->
            onUpdate("cascade");

            $table->
            foreign("remark_id")->
            references("id")->
            on("remarks")->
            onDelete("set null")->
            onUpdate("cascade");

            $table->
            foreign("user_id")->
            references("id")->
            on("users")->
            onDelete("restrict")->
            onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trucks');
    }
}
