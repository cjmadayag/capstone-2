<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("source");
            $table->string("destination");
            $table->integer("quantity");
            $table->unsignedBigInteger("remark_id")->nullable();
            $table->timestamps();
    
            $table->
            foreign("remark_id")->
            references("id")->
            on("remarks")->
            onDelete("set null")->
            onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
