<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("driver_id")->nullable();
            $table->unsignedBigInteger("trip_id")->nullable();
            $table->unsignedBigInteger("truck_tractor_id")->nullable();
            $table->unsignedBigInteger("truck_trailer_id")->nullable();
            $table->date("start")->nullable();
            $table->date("end")->nullable();
            $table->unsignedBigInteger("status_id");
            $table->unsignedBigInteger("remark_id")->nullable();
            $table->unsignedBigInteger("invoice_in_id")->nullable();
            $table->unsignedBigInteger("invoice_out_id")->nullable(); 
            $table->timestamps();
            $table->softDeletes();

            $table->
            foreign("driver_id")->
            references("id")->
            on("drivers")->
            onDelete("set null")->
            onUpdate("cascade");

            $table->
            foreign("trip_id")->
            references("id")->
            on("trips")->
            onDelete("set null")->
            onUpdate("cascade");

            $table->
            foreign("truck_tractor_id")->
            references("id")->
            on("trucks")->
            onDelete("restrict")->
            onUpdate("cascade");

            $table->
            foreign("truck_trailer_id")->
            references("id")->
            on("trucks")->
            onDelete("restrict")->
            onUpdate("cascade");

            $table->
            foreign("status_id")->
            references("id")->
            on("statuses")->
            onDelete("restrict")->
            onUpdate("cascade");

            $table->
            foreign("remark_id")->
            references("id")->
            on("remarks")->
            onDelete("set null")->
            onUpdate("cascade");

            $table->
            foreign("invoice_in_id")->
            references("id")->
            on("invoices")->
            onDelete("restrict")->
            onUpdate("cascade");

            $table->
            foreign("invoice_out_id")->
            references("id")->
            on("invoices")->
            onDelete("restrict")->
            onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
