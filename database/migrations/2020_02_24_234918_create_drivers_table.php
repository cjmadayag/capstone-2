<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name");
            $table->string("contact")->nullable();
            $table->string("address")->nullable();
            $table->unsignedBigInteger("status_id");
            $table->unsignedBigInteger("user_id");
            $table->timestamps();

            $table->
            foreign("status_id")->
            references("id")->
            on("statuses")->
            onDelete("restrict")->
            onUpdate("cascade");

            $table->
            foreign("user_id")->
            references("id")->
            on("users")->
            onDelete("restrict")->
            onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
