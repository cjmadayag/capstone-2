<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repairs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("truck_id");
            $table->unsignedBigInteger("remark_id")->nullable();
            $table->timestamps();

            $table->
            foreign("truck_id")->
            references("id")->
            on("trucks")->
            onDelete("restrict")->
            onUpdate("cascade");

            $table->
            foreign("remark_id")->
            references("id")->
            on("remarks")->
            onDelete("set null")->
            onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repairs');
    }
}
