<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("types")->insert(
            array(
                0=>array(
                    "id"=>1,
                    "name"=>"Tractor",
                    "created_at"=>null,
                    "updated_at"=>null
                ),
                1=>array(
                    "id"=>2,
                    "name"=>"Trailer",
                    "created_at"=>null,
                    "updated_at"=>null
                )
                // ,
                // 2=>array(
                //     "id"=>3,
                //     "name"=>"Source",
                //     "created_at"=>null,
                //     "updated_at"=>null
                // ),
                // 3=>array(
                //     "id"=>4,
                //     "name"=>"Destination",
                //     "created_at"=>null,
                //     "updated_at"=>null
                // )
            )
        );
    }
}
