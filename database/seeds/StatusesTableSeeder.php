<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("statuses")->insert(
            array(
                0=>array(
                    "id"=>1,
                    "name"=>"Available",
                    "created_at"=>null,
                    "updated_at"=>null
                ),
                1=>array(
                    "id"=>2,
                    "name"=>"Unavailable",
                    "created_at"=>null,
                    "updated_at"=>null
                ),
                2=>array(
                    "id"=>3,
                    "name"=>"Good Condition",
                    "created_at"=>null,
                    "updated_at"=>null
                ),
                3=>array(
                    "id"=>4,
                    "name"=>"Under Repair",
                    "created_at"=>null,
                    "updated_at"=>null
                ),
                4=>array(
                    "id"=>5,
                    "name"=>"Pending",
                    "created_at"=>null,
                    "updated_at"=>null
                ),
                5=>array(
                    "id"=>6,
                    "name"=>"In Process",
                    "created_at"=>null,
                    "updated_at"=>null
                ),
                6=>array(
                    "id"=>7,
                    "name"=>"Done",
                    "created_at"=>null,
                    "updated_at"=>null
                )
            )
        );
    }
}
