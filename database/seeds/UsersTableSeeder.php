<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert(
            array(
                0=>array(
                    "id"=>1,
                    "name"=>"Dorado",
                    "email" => "dorado@admin.com",
                    "email_verified_at" => null,
                    "password" => Hash::make("12345678"),
                    "remember_token" => null,
                    "created_at"=>null,
                    "updated_at"=>null,
                    "role_id" => 1
                ),
                1=>array(
                    "id"=>2,
                    "name"=>"PIC",
                    "email" => "pic@email.com",
                    "email_verified_at" => null,
                    "password" => Hash::make("12345678"),
                    "remember_token" => null,
                    "created_at"=>null,
                    "updated_at"=>null,
                    "role_id" => 2
                )
            )
        );
    }
}

            