<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset("css/style.css") }}" rel="stylesheet">
</head>
<body class="bg-gray-100 h-screen antialiased leading-none pt-24">
    <div id="app">
        <nav class="bg-blue-900 shadow mb-8 py-6 fixed top-0 w-full">
            <div class="container mx-auto px-6 md:px-0">
                <div class="flex items-center justify-center">
                    <div class="mr-6">
                        <a href="{{ url('/login') }}" class="text-lg font-semibold text-gray-100 no-underline">
                            {{ config('app.name') }}
                        </a>
                    </div>
                    <div class="flex">
                        @auth
                        <div>
                            <a href="/dashboard" class="hover:text-gray-500 text-white p-2 ">Dashboard</a>
                        </div> 
                        @if(Auth::user()->role_id==1)
                        <div>
                            <a href="" class="navItem hover:text-gray-500 text-white p-2">New</a>
                            <div class="hidden absolute w-20 text-center bg-gray-100 rounded overflow-hidden">
                                <a href="/adddriver" class="block border py-2 hover:bg-gray-300">Driver</a>
                                <a href="/addtruck" class="block border py-2 hover:bg-gray-300">Truck</a>
                                <a href="/addtrip" class="block border py-2 hover:bg-gray-300">Trip</a>
                            </div>
                        </div>
                        <div>
                            <a href="" class="navItem hover:text-gray-500 text-white p-2">View</a>
                            <div class="hidden absolute w-20 text-center bg-gray-100 rounded overflow-hidden">
                                <a href="/drivers" class="block border py-2 hover:bg-gray-300">Driver</a>
                                <a href="/trucks" class="block border py-2 hover:bg-gray-300">Truck</a>
                                <a href="/trips" class="block border py-2 hover:bg-gray-300">Trip</a>
                            </div>
                        </div>
                        @endif
                        @endauth
                    </div>
                    <div class="flex-1 text-right">
                        @guest
                            <a class="no-underline hover:underline text-gray-300 text-sm p-3" href="{{ route('login') }}">{{ __('Login') }}</a>
                            @if (Route::has('register'))
                                <a class="no-underline hover:underline text-gray-300 text-sm p-3" href="{{ route('register') }}">{{ __('Register') }}</a>
                            @endif
                        @else
                            <span class="text-gray-300 text-sm pr-4">{{ Auth::user()->name }}</span>

                            <a href="{{ route('logout') }}"
                               class="no-underline hover:underline text-gray-300 text-sm p-3"
                               onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                {{ csrf_field() }}
                            </form>
                        @endguest
                    </div>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
