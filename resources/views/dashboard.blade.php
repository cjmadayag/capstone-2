@extends("layouts.app")
@section("content")
{{-- <style>*{border:2px solid black}</style> --}}
@auth
<div class="container w-4/6 mx-auto flex h-full border rounded overflow-hidden shadow">
{{--     <div class="w-1/6 text-center border-r">
        <div class="grid grid-col-1 row-gap-5">
            <div class="border-b">
                <a href="" class="block py-3 hover:bg-gray-200">Dashboard</a>
                <hr>
                <a href="" class="block py-3 hover:bg-gray-200">Driver</a>
                <hr>
                <a href="" class="block py-3 hover:bg-gray-200">Truck</a>
            </div>
            <div class="border-t border-b h-64">
                
            </div>
        </div>
    </div> --}}
    <div class="">
        {{-- <div id="welcome" class="flex justify-center items-center h-64 hidden">
            <h1 class="text-4xl text-center">Welcome to your Dashboard! =)</h1>
        </div> --}}
        

        <div id="deliveryTable" class="py-4 border-b">
            <table class="table-fixed w-full text-center">
                <thead>
                    <tr>
                        <th class="py-3 text-xl" colspan="5">Deliveries</th>
                    </tr>
                    <tr>
                        <td class="font-medium py-2">Driver</td>
                        <td class="font-medium py-2">Trip</td>
                        <td class="font-medium py-2">Tractor</td>
                        <td class="font-medium py-2">Trailer</td>
                        <td class="font-medium py-2">Status</td>
                        <td class="font-medium py-2">Action</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($deliveries as $delivery)
                        <tr>
                            <form action="{{$delivery->status_id==5 ? "/updatedelivery/$delivery->id":"/removedelivery/$delivery->id"}}" method="post">
                            @csrf
                           $delivery->driver->name
                                <td class="py-1">{{$delivery->driver_id!=null?$delivery->driver->name :"No record of driver"}}</td>
                                <td class="py-1">{{$delivery->trip_id!=null?$delivery->trip->source:"No record of tractor"}} - {{$delivery->trip_id!=null?$delivery->trip->destination:""}}</td>
                                <td class="py-1">
                                    
                                    @if($delivery->truck_trailer_id != null)
                                        <span>{{$delivery->truck_tractor->plate}}</span>
                                    @else  
                                        <select class="w-1/2 bg-transparent border rounded" name="tractorId">
                                            @foreach($tractors as $tractor)
                                                <option value="{{$tractor->id}}">
                                                    {{$tractor->plate}}
                                                </option>
                                            @endforeach
                                        </select>
                                    @endif
                                </td>
                                <td class="py-1">
                                    @if($delivery->truck_trailer_id != null)
                                        <span>{{$delivery->truck_trailer->plate}}</span>
                                    @else  
                                        <select class="w-1/2 bg-transparent border rounded" name="trailerId">
                                            @foreach($trailers as $trailer)
                                                <option value="{{$trailer->id}}">
                                                    {{$trailer->plate}}
                                                </option>
                                            @endforeach
                                        </select>
                                    @endif
                                </td>
                                <td class="py-1">{{$delivery->status->name}}</td>
                                @if(Auth::user()->role_id!=3)
                                <td class="py-1">
                                    @if($delivery->status_id==5)
                                        @method("patch")
                                        <button class="bg-blue-300 p-1 rounded">Deliver</button>
                                    @else
                                        @method("delete")
                                        <button type="submit" class="bg-blue-300 p-1 rounded">Done</button>
                                    @endif
                                </td>
                                @endif
                            </form> 
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="flex h-full">
            <div id="tripTable" class="w-1/2 border-l">
                <table class="table-fixed w-full text-center">
                    <thead>
                        <tr>
                            <th class="py-3 text-base" colspan="3">Trips</th>
                        </tr>
                        {{-- <tr>
                            <td class="py-3 font-bold">Source</td>
                            <td class="py-3 font-bold">Destination</td>
                            <td class="py-3"></td>
                        </tr> --}}
                    </thead>
                    <tbody>

                        @foreach($trips as $trip)
                        
                        <tr>
                            <td class="py-3">{{$trip->source}}</td>
                            <td class="py-3">{{$trip->destination}}</td>
                            <td class="py-3">
                                <div class="">
                                    @if(Auth::user()->role_id==1)
                                        @if($trip->quantity!=0)
                                            <span class="pr-5 text-lg">
                                                {{$trip->quantity}}
                                            </span>
                                            <a href="/addqtytotrip/{{$trip->id}}/+" class="border py-1 px-3 rounded bg-blue-300">+</a>
                                            <a href="/addqtytotrip/{{$trip->id}}/-" class="border py-1 px-3 rounded bg-gray-400">-</a>
                                        @else
                                            <a href="/addqtytotrip/{{$trip->id}}/+" class="border px-2 py-1 rounded bg-blue-300">Add</a>
                                        @endif
                                    @else
                                        <span class="pr-5 text-lg">
                                            {{$trip->quantity}}
                                        </span>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>

            <div id="driverTable" class="w-1/2">
                <table class="table-fixed w-full text-center">
                    <thead>
                        <tr>
                            <th class="py-3 text-base" colspan="2">Drivers</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($drivers as $driver)
                            <tr>
                                <td>{{$driver->name}}</td>
                                <td>
                                
                                    
                                    @if($openTrips!=null)
                                    <button class="border p-2 bg-blue-300 rounded" onclick="assign({{$driver->id}})">Assign</button>
                                    @else

                                    <span>Available</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="fixed top-0 h-screen w-screen flex justify-center items-center hidden">
    <div class="bg-gray-500 opacity-50 h-screen w-screen absolute">

    </div>
    <div class="h-64 w-1/3 rounded bg-white overflow-hidden shadow z-0">


        <h2 class="text-center text-3xl bg-blue-300 py-2"></h2>
   
        

        <table id="modal" class="w-full text-2xl text-center table-fixed mt-2">
            <tbody>
                @foreach ($openTrips as $openTrip)
                <tr>
                    <td colspan="3" class="border">{{$openTrip->source}}</td>
                    <td colspan="3" class="text-left border">{{$openTrip->destination}}</td>
                    <td>
                        <input type="radio" name="trip_id" value="{{$openTrip->id}}">
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7" class="text-right">
                        <form action="/adddelivery" method="post">
                            @csrf
                            <input type="hidden" name="driverId">
                            <input type="hidden" name="tripId">
                            <button class="border py-2 px-1 mt-4 rounded text-lg bg-blue-200" id="assign" type="submit">Assign</button>
                            <button type="button" class="border py-2 px-1 mt-4 rounded text-lg bg-gray-400">Cancel</button>
                        </form>
                    </td>
                </tr>
            </tfoot>
        </table>
        
    </div>
</div>  
<script src="{{asset("js/dashboard.js")}}"></script>
@endauth
@endsection