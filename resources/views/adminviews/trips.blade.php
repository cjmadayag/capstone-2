@extends("layouts.app")
@section("content")
<h1 class="text-center text-2xl font-medium pb-3">Trips</h1>
<div class="container lg:w-3/4 mx-auto">
    <table class="table-auto w-full text-center">
        <thead>
            <tr>
                <td class="w-1/3 border py-3 font-medium">Source</td>
                <td class="w-1/3 border py-3 font-medium">Destination</td>
                <td class="w-1/3 border py-3 font-medium">Destination</td>
            </tr>
        </thead> 
        <tbody>
            @foreach($trips as $trip)
            <tr>
                <td class="border py-3">{{$trip->source}}</td>
                <td class="border py-3">{{$trip->destination}}</td>
                <td class="border">
                    <div class="flex justify-center items-center">
                        <form action="/edittrip/{{$trip->id}}">
                            <button class="border py-2 px-3 border bg-blue-200 rounded">Edit</button>
                        </form>
                        <form action="/removetrip/{{$trip->id}}" method="post">
                            @csrf
                            @method("delete")
                            <button class="border py-2 px-3 border bg-gray-400 rounded">Remove</button>
                        </form>
                    </div>
                    
                </td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
</div>
@endsection