@extends("layouts.app")
@section("content")
<div class="w-2/6 mx-auto border rounded">
    <h1 class="text-center pt-5 text-2xl font-medium">Edit Driver</h1>
    <form action="/editdriver/{{$driver->id}}" class="p-5" method="post">
        @csrf
        @method("patch")
        <div class="pb-1">
            <label for="name" class="block py-1 ml-2">Name</label>
        <input type="text" id="name" name="name" class="w-full border py-1" value="{{$driver->name}}">
        </div>
        <div class="py-1">
            <label for="contact" class="block py-1 ml-2">Contact</label>
            <input type="text" id="contact" name="contact" class="w-full border py-1" value="{{$driver->contact}}">
        </div>
        <div class="py-1">
            <label for="address" class="block py-1 ml-2">Address</label>
            <input type="text" id="address" name="address" class="w-full border py-1" value="{{$driver->address}}">
        </div>
        <div class="py-1">
            <label for="user_id" class="block py-1 ml-2">Handler</label>
            <select name="user_id" id="user_id" class="w-full border bg-white py-1">
                @foreach($handlers as $handler)
                    <option value="{{$handler->id}}" {{$handler->id==$driver->user_id?"selected":""}}>{{$handler->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="pt-3 text-center">
            <button class="py-2 px-5 rounded bg-green-200">Update</button>
            <button type="button" id="clear" class="py-2 px-5 rounded bg-gray-400">Clear</button>
        </div>
    </form>
</div>
<script src="{{asset("js/form.js")}}"></script>
@endsection