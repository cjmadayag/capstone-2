@extends("layouts.app")
@section("content")
<h1 class="text-center text-2xl font-medium pb-3">Trucks</h1>
<div class="container lg:w-3/4 mx-auto">
    <table class="table-auto w-full text-center">
        <thead>
            <tr>
                <td class="w-1/5 border py-3 font-medium">Plate</td>
                <td class="w-1/5 border py-3 font-medium">Type</td>
                <td class="w-1/5 border py-3 font-medium">Status</td>
                <td class="w-1/5 border py-3 font-medium">Handler</td>
                <td class="w-1/5 border py-3 font-medium"></td>
            </tr>
        </thead> 
        <tbody>
            @foreach($trucks as $truck)
            <tr>
                <td class="border py-3">{{$truck->plate}}</td>
                <td class="border py-3">{{$truck->type->name}}</td>
                <td class="border py-3">{{$truck->status->name}}</td>
                <td class="border py-3">{{$truck->user->name}}</td>
                <td class="border">
                    <div class="flex justify-center items-center">
                        <form action="/edittruck/{{$truck->id}}">
                            <button class="border py-2 px-3 border bg-blue-200 rounded">Edit</button>
                        </form>
                        <form action="/removetruck/{{$truck->id}}" method="post">
                            @csrf
                            @method("delete")
                            <button class="border py-2 px-3 border bg-gray-400 rounded">Remove</button>
                        </form>
                    </div>
                    
                </td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
</div>
@endsection