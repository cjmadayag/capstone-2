@extends("layouts.app")
@section("content")
<div class="w-1/4 mx-auto border rounded">
    <h1 class="text-center pt-5 text-2xl font-medium">Add Trip</h1>
    <form action="" class="p-5" method="post">
        @csrf
        <div class="pb-1">
            <label for="source" class="block py-1 ml-2">Source</label>
            <input type="text" id="source" name="source" class="w-full py-1 border" >
        </div>
        <div class="py-1">
            <label for="destination" class="block py-1 ml-2">Destination</label>
            <input type="text" id="destination" name="destination" class="w-full py-1 border" >
        </div>
        <div class="pt-3 text-center">
            <button class="py-2 px-5 rounded bg-green-200">Add</button>
            <button type="button" id="clear" class="py-2 px-5 rounded bg-gray-400">Clear</button>
        </div>
    </form>
</div>
<script src="{{asset("js/form.js")}}"></script>
@endsection