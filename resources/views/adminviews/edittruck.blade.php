@extends("layouts.app")
@section("content")
<div class="w-2/6 mx-auto border rounded">
    <h1 class="text-center pt-5 text-2xl font-medium">Edit Truck</h1>
    <form action="/edittruck/{{$truck->id}}" class="p-5" method="post">
        @csrf
        @method("patch")
        <div class="pb-1">
            <label for="plate" class="block py-1 ml-2">Plate</label>
            <input type="text" id="plate" name="plate" class="w-full border py-1" value="{{$truck->plate}}">
        </div>
        <div class="py-1">
            <label for="type_id" class="block py-1 ml-2">Type</label>
            <select name="type_id" id="type_id" class="w-full border py-1 bg-white">
                @foreach($types as $type)
                    <option value="{{$type->id}}" {{$type->id == $truck->type_id? "selected":""}}>{{$type->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="py-1">
            <label for="user_id" class="block py-1 ml-2">Handler</label>
            <select name="user_id" id="user_id" class="w-full border bg-white py-1">
                @foreach($handlers as $handler)
                    <option value="{{$handler->id}}" {{$handler->id==$truck->user_id? "selected":""}}>{{$handler->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="pt-3 text-center">
            <button class="py-2 px-5 rounded bg-green-200">Update</button>
            <button type="button" id="clear" class="py-2 px-5 rounded bg-gray-400">Clear</button>
        </div>
    </form>
</div>
<script src="{{asset("js/form.js")}}"></script>
@endsection