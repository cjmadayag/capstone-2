@extends("layouts.app")
@section("content")
<h1 class="text-center text-2xl font-medium pb-3">Drivers</h1>
<div class="container lg:w-3/4 mx-auto">
    <table class="table-auto w-full text-center">
        <thead>
            <tr>
                <td class="w-1/5 border py-3 font-medium">Name</td>
                <td class="w-1/5 border py-3 font-medium">Contact</td>
                <td class="w-1/5 border py-3 font-medium">Address</td>
                <td class="w-1/5 border py-3 font-medium">Handler</td>
                <td class="w-1/5 border py-3 font-medium"></td>
            </tr>
        </thead> 
        <tbody>
            @foreach($drivers as $driver)
            <tr>
                <td class="border py-3">{{$driver->name}}</td>
                <td class="border py-3">{{$driver->contact}}</td>
                <td class="border py-3">{{$driver->address}}</td>
                <td class="border py-3">{{$driver->user->name}}</td>
                <td class="border">
                    <div class="flex justify-center items-center">
                        <form action="/editdriver/{{$driver->id}}">
                            <button class="border py-2 px-3 border bg-blue-200 rounded">Edit</button>
                        </form>
                        <form action="/removedriver/{{$driver->id}}" method="post">
                            @csrf
                            @method("delete")
                            <button class="border py-2 px-3 border bg-gray-400 rounded">Remove</button>
                        </form>
                    </div>
                </td>
            </tr>
            @endforeach
            
        </tbody>
    </table>
</div>
@endsection