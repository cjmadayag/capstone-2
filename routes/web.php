<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/dashboard', function () {
//     return view('dashboard');
// });
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index')->name('home');

// Dashboard
Route::get("/dashboard", "DeliveryController@index");

// Driver
Route::get("/adddriver", "DriverController@create");
Route::post("/adddriver", "DriverController@store");
// View Drivers
Route::get("/drivers", "DriverController@index");
// Edit Driver
Route::get("/editdriver/{id}", "DriverController@edit");
Route::patch("/editdriver/{id}", "DriverController@update");
// Delete Driver
Route::delete("/removedriver/{id}", "DriverController@destroy");

// Truck
Route::get("/addtruck", "TruckController@create");
Route::post("/addtruck", "TruckController@store");
// View Trucks
Route::get("/trucks", "TruckController@index");
// Edit Truck
Route::get("/edittruck/{id}", "TruckController@edit");
Route::patch("/edittruck/{id}", "TruckController@update");
// Delete Truck
Route::delete("/removetruck/{id}", "TruckController@destroy");

// Trip
Route::get("/addtrip", "TripController@create");
Route::post("/addtrip", "TripController@store");
// View Trips
Route::get("/trips", "TripController@index");
// Edit Trip
Route::get("/edittrip/{id}", "TripController@edit");
Route::patch("/edittrip/{id}", "TripController@update");
// Delete Trip
Route::delete("/removetrip/{id}", "TripController@destroy");

// Dashboard
// Fetch
Route::get("/fetchDashboard", "DeliveryController@fetchDashboard");
Route::get("/fetchDelivery", "DeliveryController@fetchDelivery");
Route::get("/fetchDriver", "DriverController@fetchDriver");
Route::get("/fetchTrip", "TripController@fetchTrip");

Route::get("/getDriver/{id}", "DriverController@getDriver");

// Add Qty to Trip
Route::get("/addqtytotrip/{id}/{opt}","TripController@addQtyToTrip");

// Add Delivery
Route::post("/adddelivery","DeliveryController@addDelivery");

// Update Delivery
Route::patch("/updatedelivery/{id}","DeliveryController@updateDelivery");

// Delete Delivery
Route::delete("/removedelivery/{id}","DeliveryController@destroyDelivery");


